import {ADD_ATTENDANT} from '../actions/actionTypes';

function reducer(state = {}, action) {
    switch (action.type) {
        case ADD_ATTENDANT:
            return Object.assign({}, state, {
                addAttendantPayload: action.addAttendantPayload,
            })
        default:
            return state;
    }
}
export default reducer;