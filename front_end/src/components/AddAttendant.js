import React, {Component} from 'react';
import {connect} from 'react-redux'
import {addAttendant} from '../actions/actions'
import {Button, Form, Message, Segment} from 'semantic-ui-react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class AddAttendant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            firstName: '',
            secondName: '',
            eventDate: new Date(),
            email: '',
            error: false,
            errors: {},
            success: false,
            successMsg: ''
        };
    }

    submitForm() {
        this.setState({loading: true},
            () => {
                this.props.addAttendant({
                    firstName: this.state.firstName,
                    secondName: this.state.secondName,
                    email: this.state.email,
                    eventDate: this.state.eventDate.toISOString().slice(0, 10)
                })
            }
        )
    }

    componentDidUpdate(prevProps) {
        if (prevProps.addAttendantPayload !== this.props.addAttendantPayload) {
            if (this.props.addAttendantPayload.errors !== undefined) {
                let errors = {};
                this.props.addAttendantPayload.errors.forEach(function (arrayItem) {
                    errors[arrayItem.param] = arrayItem.msg;
                });

                this.setState({
                    error: true,
                    errors: errors,
                    success: false
                });
            } else {

                this.setState({
                    error: false,
                    errors: {},
                    success: this.props.addAttendantPayload.success,
                    successMsg: this.props.addAttendantPayload.msg
                }, () => this.resetFields());
            }
        }
    }

    resetFields() {
        this.setState({
            firstName: '',
            secondName: '',
            eventDate: new Date(),
            email: '',
        });
    }

    onInputChange(name, e) {
        let change = {};
        change[name] = e.target.value;
        this.setState(change);
    }

    handleDateChange = date => {
        this.setState({
            eventDate: date
        });
    };

    render() {
        let state = this.state;
        return (
            <Segment basic>
                <Form onSubmit={this.submitForm.bind(this)}>
                    <Form.Field>
                        <label>First Name</label>
                        <Form.Input
                            required
                            style={{display: "block"}}
                            value={state.firstName}
                            onChange={this.onInputChange.bind(this, 'firstName')}
                            placeholder='First Name'
                            error={this.state.errors.firstName}
                        />

                    </Form.Field>

                    <Form.Field>
                        <label>Second Name</label>
                        <Form.Input
                            required
                            style={{display: "block"}}
                            value={state.secondName}
                            onChange={this.onInputChange.bind(this, 'secondName')}
                            placeholder='Second Name'
                            error={this.state.errors.secondName}
                        />
                    </Form.Field>

                    <Form.Field>
                        <label>Email</label>
                        <Form.Input
                            type='email'
                            required
                            style={{display: "block"}}
                            value={state.email}
                            onChange={this.onInputChange.bind(this, 'email')}
                            placeholder='Email'
                            error={this.state.errors.email}
                        />
                    </Form.Field>

                    <Form.Field>
                        <label>Date</label>
                        <DatePicker
                            required
                            dateFormat="yyyy-MM-dd"
                            selected={this.state.eventDate}
                            onChange={this.handleDateChange}
                        />
                    </Form.Field>

                    <Message
                        positive
                        compact
                        success={!this.state.success}
                        header='Success!'
                        content={this.state.successMsg}
                    />
                    <br/>
                    <Button primary type='submit'>Submit</Button>
                </Form>
            </Segment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addAttendant: data => dispatch(addAttendant(data))
    };
}

const mapStateToProps = state => {
    return {addAttendantPayload: state.addAttendantPayload};
};

const Attendant = connect(
    mapStateToProps,
    mapDispatchToProps
)(AddAttendant);

export default Attendant;