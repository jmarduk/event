import React, { Component } from 'react';

import './App.css';
import AddAttendant from "./components/AddAttendant";
import 'semantic-ui-css/semantic.min.css'


class App extends Component {
    render() {
        return (
            <div className="App">

                <AddAttendant />

            </div>
        );
    }
}

export default App;
