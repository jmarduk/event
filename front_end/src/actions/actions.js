import {ADD_ATTENDANT, PATH} from './actionTypes'
import axios from 'axios';

export const addAttendant = (payload) => {

    return dispatch => {
        return axios
            .post(PATH + '/attendant', payload)
            .then(res => {
                dispatch(addTodoSuccess(res.data));
            })
            .catch(err => {
                dispatch(addTodoFailure(err.response.data));
            });
    };
};

const addTodoSuccess = payload => ({
    type: ADD_ATTENDANT,
    addAttendantPayload:
    payload
});

const addTodoFailure = error => ({
    type: ADD_ATTENDANT,
    addAttendantPayload:
    error
});


