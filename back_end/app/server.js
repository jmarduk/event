#!/usr/bin/env node
const app = require('./index')
const config = require('./config')

app.listen(config.express.port, config.express.ip, function (error) {
  if (error) {
    process.exit(10)
  }
})
