module.exports = [
    function notFound(req, res) {
        res.status(404).json({ success: false, error: 'Resource not found' });
    },

    function errorHandler(error, req, res, next) {
     res.status(500).json({ success: false, error: 'Something went wrong.' });
    }
]