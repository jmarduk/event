const config = module.exports
const PRODUCTION = process.env.NODE_ENV === 'production'

if (PRODUCTION) {
  config.express = {
    port: process.env.EXPRESS_PORT || 4040,
    ip: '127.0.0.1'
  }
  config.mongodb = {
    port: process.env.MONGODB_PORT || 27017,
    host: process.env.MONGODB_HOST || 'localhost',
    databaseName: 'event'
  }
} else {
  config.express = {
    port: 6060,
    ip: '127.0.0.1'
  }
  config.mongodb = {
    port: process.env.MONGODB_PORT || 27017,
    host: process.env.MONGODB_HOST || 'localhost',
    databaseName: 'event_testing'
  }
}
